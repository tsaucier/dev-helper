#!/bin/bash

IMAGE_VERSION=${1:-latest}

docker build -t registry.gitlab.com/tsaucier/dev-helper:$IMAGE_VERSION .
docker push registry.gitlab.com/tsaucier/dev-helper:$IMAGE_VERSION
