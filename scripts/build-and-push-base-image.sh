#!/bin/bash

IMAGE_VERSION=22.04

docker pull ubuntu:$IMAGE_VERSION
docker tag ubuntu:$IMAGE_VERSION registry.gitlab.com/tsaucier/dev-helper/ubuntu:$IMAGE_VERSION
docker push registry.gitlab.com/tsaucier/dev-helper/ubuntu:$IMAGE_VERSION
