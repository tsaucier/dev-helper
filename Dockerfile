FROM registry.gitlab.com/tsaucier/dev-helper/ubuntu:22.04 as getter

WORKDIR /tmp

RUN apt update && apt install -y \
    wget \
    apt-transport-https \
    software-properties-common && \
    wget -q "https://packages.microsoft.com/config/ubuntu/$(lsb_release -rs)/packages-microsoft-prod.deb"

## main dev image
FROM registry.gitlab.com/tsaucier/dev-helper/ubuntu:22.04

ARG DEV_HELPER_USER=devhelper
ENV DEBIAN_FRONTEND=noninteractive

COPY --from=getter /tmp/packages-microsoft-prod.deb packages-microsoft-prod.deb

RUN apt update && apt install -y \
    wget \
    curl \
    git \
    make \
    vim \
    apt-transport-https \
    software-properties-common && \
    ## deps for powershell7
    dpkg -i packages-microsoft-prod.deb && \
    rm packages-microsoft-prod.deb && \
    ## deps for python
    add-apt-repository ppa:deadsnakes/ppa && \
    ## install third party packages
    apt update && apt install -y \
      powershell \
      python3.10 && \
    ## add new user
    useradd -m $DEV_HELPER_USER

USER $DEV_HELPER_USER

RUN git clone --depth=1 https://github.com/tfutils/tfenv.git ~/.tfenv && \
    echo 'export PATH="$HOME/.tfenv/bin:$PATH"' >> ~/.bashrc

WORKDIR /home/$DEV_HELPER_USER
