# dev-helper
This image is to help bridge the gap between the workstations / operating systems being used and having one image with all the 
tools needed for successful development / debugging. 

## Overview
See the [Dockerfile](./Dockerfile) for a list of packages this image contains, here are some key third party packages included: 
* PowerShell 7
* Python 3.10
* tfenv

The image executes under a non-root user named _devhelper_

## Getting started
You can run the image by executing in your terminal:   
```shell
docker run -it registry.gitlab.com/tsaucier/dev-helper:latest
```

If you want to mount a volume (make sure the volume exists before mounting): 
```shell
docker run \
  -it \
  -v $PWD/some-volume/:/home/devhelper/some-volume \
  registry.gitlab.com/tsaucier/dev-helper:latest
```
